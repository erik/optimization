#ifndef OPTIMIZATION_OBJECTIVES_ROSENBROCK_H
#define OPTIMIZATION_OBJECTIVES_ROSENBROCK_H

#include "utils/vector.h"

namespace optimization {

//--------------------------------------------------------------------------------------------------
template <int32_t N>
class Rosenbrock {
public:
    static constexpr char const* name = "rosenbrock";

    Rosenbrock() {}

    void operator()(VectorNs<N> const& x, Scalar& value) {
        uint32_t const dim = x.size();
        value = Scalar(0);
        for (uint32_t i = 1; i < dim; ++i) {
            Scalar const x_squared = x[i - 1] * x[i - 1];
            Scalar const tmp_1 = (1 - x[i - 1]);
            Scalar const tmp_2 = (x[i] - x_squared);
            value += tmp_1 * tmp_1 + 100 * tmp_2 * tmp_2;
        }
    }

    void operator()(VectorNs<N> const& x, Scalar& value, VectorNs<N>& gradient) {
        uint32_t const dim = x.size();
        value = Scalar(0);
        gradient.resize(dim);
        gradient.setZero();
        for (uint32_t i = 1; i < dim; ++i) {
            Scalar const x_squared = x[i - 1] * x[i - 1];
            Scalar const tmp_1 = (1 - x[i - 1]);
            Scalar const tmp_2 = (x[i] - x_squared);
            value += tmp_1 * tmp_1 + 100 * tmp_2 * tmp_2;
            gradient[i - 1] += -2 * tmp_1 - 400 * tmp_2 * x[i - 1];
            gradient[i] += 200 * tmp_2;
        }
    }
};

}

#endif
