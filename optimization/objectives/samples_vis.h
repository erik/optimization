#ifndef OPTIMIZATION_OBJECTIVES_SAMPLES_VIS_H
#define OPTIMIZATION_OBJECTIVES_SAMPLES_VIS_H

#include "samples.h"
#include "utils/eigen_json.h"

namespace optimization {

//..................................................................................................
template <typename Vector>
void to_json(nlohmann::json& j, GradientSample<Vector> const& sample) {
    j = nlohmann::json{
        {"point", sample.point},
        {"value", sample.value},
        {"gradient", sample.gradient}
    };
}

}

#endif
