#ifndef OPTIMIZATION_OBJECTIVES_PARABOLOID_H
#define OPTIMIZATION_OBJECTIVES_PARABOLOID_H

#include "utils/scalar.h"
#include "utils/vector.h"

namespace optimization {

//--------------------------------------------------------------------------------------------------
template <typename Vector>
class Paraboloid {
public:
    using Input = Vector;
    using Gradient = Vector;
    static constexpr char const* name = "paraboloid";

    Paraboloid() {}

    void operator()(Input const& x, Scalar& value) const {
        uint32_t const dim = x.size();
        value = 0;
        for (uint32_t d = 0; d < dim; ++d) {
            value += x[d] * x[d];
        }
    }

    void operator()(Input const& x, Scalar& value, Gradient& gradient) const {
        uint32_t const dim = x.size();
        value = 0;
        for (uint32_t d = 0; d < dim; ++d) {
            value += x[d] * x[d];
            gradient[d] = 2 * x[d];
        }
    }
};

}

#endif
