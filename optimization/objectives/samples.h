#ifndef OPTIMIZATION_OBJECTIVES_SAMPLES_H
#define OPTIMIZATION_OBJECTIVES_SAMPLES_H

#include "utils/scalar.h"

namespace optimization {

//--------------------------------------------------------------------------------------------------
template <typename Vector>
struct Sample {
    Sample() {}
    Sample(Vector const& p, Scalar v)
        : point(p), value(v)
    {}

    Vector point;
    Scalar value;
};

//--------------------------------------------------------------------------------------------------
template <typename Vector>
struct GradientSample {
    GradientSample() {}
    GradientSample(Vector const& p, Scalar v, Vector const& g)
        : point(p), value(v), gradient(g)
    {}

    Vector point;
    Scalar value;
    Vector gradient;
};

}

#endif
