#ifndef OPTIMIZATION_GRADIENT_DESCENT_VIS_H
#define OPTIMIZATION_GRADIENT_DESCENT_VIS_H

#include <iostream>
#include "everything.h"

namespace optimization {

//--------------------------------------------------------------------------------------------------
// Provides an way to serialize a subset of an everything log that is compatible with the
// visualization script.
template <int32_t N>
struct GradientDescentVis {
    GradientDescentLogEverything<N> const& log;
};

//..................................................................................................
template <int32_t N>
void to_json(nlohmann::json& j, GradientDescentVis<N> const& vis) {
    j = nlohmann::json{
        {"algorithm", "gradient descent"},
        {"objective", vis.log.objective_name},
        {"data", {
            {"points", nlohmann::json::array()}
        }}
    };
    for (auto const& sample : vis.log.samples) {
        j["data"]["points"].push_back(sample.point);
    }
}

}

#endif
