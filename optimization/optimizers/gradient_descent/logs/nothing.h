#ifndef OPTIMIZATION_GRADIENT_DESCENT_LOGS_NOTHING_H
#define OPTIMIZATION_GRADIENT_DESCENT_LOGS_NOTHING_H

#include "utils/vector.h"

namespace optimization {

//--------------------------------------------------------------------------------------------------
struct GradientDescentLogNothing {
    template <typename Objective>
    void initialize(Objective const&) {}

    template <int32_t N>
    void push_back(VectorNs<N> const&, Scalar, VectorNs<N> const&) {}
};

}

#endif
