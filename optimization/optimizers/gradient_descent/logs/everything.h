#ifndef OPTIMIZATION_GRADIENT_DESCENT_LOGS_EVERYTHING_H
#define OPTIMIZATION_GRADIENT_DESCENT_LOGS_EVERYTHING_H

#include "objectives/samples.h"
#include "objectives/samples_vis.h"
#include "utils/eigen_json.h"
#include "utils/vector.h"
#include <string>
#include <vector>

namespace optimization {

//--------------------------------------------------------------------------------------------------
// This is used as a base class rather than a member so that the empty base class optimization can
// be applied (the member would take up space even if it is an empty class).
template <int32_t N>
struct GradientDescentLogEverything {
    template <typename Objective>
    void initialize(Objective const&) { objective_name = Objective::name; }
    void push_back(
        VectorNs<N> const& point,
        Scalar value,
        VectorNs<N> const& gradient
    ) {
        samples.emplace_back(point, value, gradient);
    }
    void clear();

    std::string objective_name;
    std::vector<GradientSample<VectorNs<N>>> samples;
};

//--------------------------------------------------------------------------------------------------
template <int32_t N>
void to_json(nlohmann::json& j, GradientDescentLogEverything<N> const& log) {
    j = nlohmann::json{
        {"algorithm", "gradient descent"},
        {"objective", log.objective_name},
        {"data", log.samples}
    };
}

}

#endif
