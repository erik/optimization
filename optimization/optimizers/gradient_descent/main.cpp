#include "clara.hpp"
#include "gradient_descent.h"
#include "logs/everything.h"
#include "logs/everything_vis.h"
#include "objectives/paraboloid.h"
#include "objectives/rosenbrock.h"
#include "utils/eigen_json.h"
#include <fstream>
#include <iostream>

using namespace optimization;
using json = nlohmann::json;

//--------------------------------------------------------------------------------------------------
int main(int const argc, char const** argv) {
    std::string log_file_path;
    std::string vis_file_path;
    uint32_t dim = 2;
    Scalar learning_rate = 0.0015;
    uint32_t max_evaluations = 1000;
    Scalar gradient_threshold = 1e-8;
    Scalar x0 = -1;
    Scalar y0 = 2;

    auto const cli =
        clara::Arg(log_file_path, "log_file_path")("Location of the optimization log") |
        clara::Arg(vis_file_path, "vis_file_path")("Location of the visualization file") |
        clara::Opt(dim, "dim")["-d"]["--dimension"]("Dimensionality of the objective") |
        clara::Opt(learning_rate, "learning_rate")["-l"]["--learning-rate"]("Gradient descent learning rate") |
        clara::Opt(max_evaluations, "max_evaluations")["-n"]["--max-evaluations"]("Max number of gradient descent steps") |
        clara::Opt(gradient_threshold, "gradient_threshold")["-g"]["--gradient-threshold"]("Terminate if the gradient norm is this small") |
        clara::Opt(x0, "x0")["-x"]["--x0"]("X coordinate of initial point") |
        clara::Opt(y0, "y0")["-y"]["--y0"]("Y coordinate of initial point");
    auto const result = cli.parse(clara::Args(argc, argv));
    if (!result) {
        std::cerr << "Error in command line: " << result.errorMessage() << std::endl;
        exit(1);
    }

    VectorXs initial_point;
    initial_point.resize(dim);
    initial_point[0] = x0;
    initial_point[1] = y0;
    for (uint32_t i = 2; i < dim; ++i) {
        initial_point[i] = -1;
    }

    //using Objective = Paraboloid<Vector2<Scalar>>;
    using Objective = Rosenbrock<-1>;
    Objective objective;

    GradientDescent<-1> optimizer(learning_rate, max_evaluations, gradient_threshold);
    GradientDescentLogEverything<-1> log;

    // Only log stuff if we're going to use it.
    if (log_file_path.empty() && vis_file_path.empty()) {
        optimizer.optimize(objective, initial_point);
    } else {
        optimizer.optimize(objective, initial_point, log);
    }

    std::cout << "n evaluations: " << optimizer.n_evaluations() << '\n';
    std::cout << "final point: " << optimizer.point() << '\n';

    if (!log_file_path.empty()) {
        json data = log;
        std::ofstream log_file(log_file_path);
        log_file << data.dump(4) << '\n';
    }

    if (!vis_file_path.empty()) {
        json data = GradientDescentVis<-1>{log};
        std::ofstream vis_file(vis_file_path);
        vis_file << data.dump(4) << '\n';
    }

    return 0;
}
