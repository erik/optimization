#ifndef OPTIMIZATION_GRADIENT_DESCENT_H
#define OPTIMIZATION_GRADIENT_DESCENT_H

#include "logs/nothing.h"
#include <iostream>

namespace optimization {

//--------------------------------------------------------------------------------------------------
template <int32_t N>
class GradientDescent {
public:
    GradientDescent() {}
    GradientDescent(Scalar learning_rate, uint32_t me, Scalar gt)
        : learning_rate_(learning_rate), max_evaluations_(me), gradient_threshold_(gt)
    {}

    Scalar& learning_rate() { return learning_rate_; }
    Scalar learning_rate() const { return learning_rate_; }

    uint32_t n_evaluations() const { return n_evaluations_; }
    // We evaluate the objective function exactly once per iteration.
    uint32_t n_iterations() const { return n_evaluations_; }
    VectorNs<N> const& point() const { return point_; }
    VectorNs<N> const& gradient() const { return gradient_; }
    Scalar value() const { return value_; }

    // TODO: Support passing initial point by rvalue reference.
    template <typename Objective>
    VectorNs<N> const& optimize(Objective& objective, VectorNs<N> const& initial_point);

    template <typename Objective, typename Log>
    VectorNs<N> const& optimize(Objective& objective, VectorNs<N> const& initial_point, Log& log);

private:
    // hyperparameters
    Scalar learning_rate_;
    uint32_t max_evaluations_;
    Scalar gradient_threshold_;

    // algorithm state
    uint32_t n_evaluations_;
    VectorNs<N> point_;
    VectorNs<N> gradient_;
    Scalar value_;
};

//..................................................................................................
template <int32_t N>
template <typename Objective>
VectorNs<N> const& GradientDescent<N>::optimize(
    Objective& objective,
    VectorNs<N> const& initial_point
) {
    GradientDescentLogNothing log;
    return optimize(objective, initial_point, log);
}

//..................................................................................................
template <int32_t N>
template <typename Objective, typename Log>
VectorNs<N> const& GradientDescent<N>::optimize(
    Objective& objective,
    VectorNs<N> const& initial_point,
    Log& log
) {
    n_evaluations_ = 0;
    point_ = initial_point;
    gradient_.resize(point_.size());
    log.initialize(objective);

    while (true) {
        objective(point_, value_, gradient_);
        ++n_evaluations_;
        log.push_back(point_, value_, gradient_);

        // termination conditions
        if (n_evaluations_ >= max_evaluations_) {
            std::cout << "Gradient descent reached max evaluations: " << n_evaluations_ << '\n';
            break;
        }
        if (gradient_.norm() <= gradient_threshold_) {
            std::cout << "Gradient descent reached gradient threshold: " << gradient_.norm();
            std::cout << '\n';
            break;
        }

        point_ -= learning_rate_ * gradient_;
    }

    return point_;
}

}

#endif
