add_executable(gradient_descent
    main.cpp
)
target_link_libraries(gradient_descent optimization_lib clara)

make_plot_target(gradient_descent 2d ARGS -d 2 -l 0.0015 -n 10000)
make_plot_target(gradient_descent 10d ARGS -d 10 -l 0.0005 -n 10000)
