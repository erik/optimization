#include "clara.hpp"
#include "conjugate_gradient_descent.h"
#include "logs/everything_vis.h"
#include "objectives/paraboloid.h"
#include "objectives/rosenbrock.h"
#include "utils/eigen_json.h"
#include <fstream>
#include <iostream>

using namespace optimization;
using json = nlohmann::json;

//--------------------------------------------------------------------------------------------------
int main(int const argc, char const** argv) {
    std::string log_file_path;
    std::string vis_file_path;
    uint32_t dim = 2;
    Scalar gradient_threshold = 1e-8;
    Scalar progress_threshold = 1e-8;
    uint32_t max_evaluations = 10000;
    uint32_t max_iterations = 1000;
    Scalar x0 = -1;
    Scalar y0 = 2;

    auto const cli =
        clara::Arg(log_file_path, "log_file_path")("Location of the optimization log") |
        clara::Arg(vis_file_path, "vis_file_path")("Location of the visualization file") |
        clara::Opt(dim, "dim")["-d"]["--dim"]("Dimension of the search space") |
        clara::Opt(gradient_threshold, "gradient_threshold")["-g"]["--gradient"]("Return if the gradient norm is this small") |
        clara::Opt(progress_threshold, "progress_threshold")["-p"]["--progress"]("Return if the difference between two consecutive values is this small") |
        clara::Opt(max_evaluations, "max_evaluations")["-n"]["--max-evaluations"]("Maximum number of function evaluations") |
        clara::Opt(max_iterations, "max_iterations")["-i"]["--max-iterations"]("Maximum number of line minimization cycles") |
        clara::Opt(x0, "x0")["-x"]["--x0"]("X coordinate of initial point") |
        clara::Opt(y0, "y0")["-y"]["--y0"]("Y coordinate of initial point");
    auto const result = cli.parse(clara::Args(argc, argv));
    if (!result) {
        std::cerr << "Error in command line: " << result.errorMessage() << std::endl;
        exit(1);
    }

    VectorXs initial_point;
    initial_point.resize(dim);
    initial_point[0] = x0;
    initial_point[1] = y0;
    for (uint32_t i = 2; i < dim; ++i) {
        initial_point[i] = -1;
    }

    //using Objective = Paraboloid<Vector2<Scalar>>;
    using Objective = Rosenbrock<-1>;
    Objective objective;

    ConjugateGradientDescent<-1> optimizer(
        gradient_threshold,
        progress_threshold,
        max_evaluations,
        max_iterations
    );
    ConjugateGradientDescentLogEverything<-1> log;

    // Only log stuff if we're going to use it.
    if (log_file_path.empty() && vis_file_path.empty()) {
        optimizer.optimize(objective, std::move(initial_point));
    } else {
        optimizer.optimize(objective, std::move(initial_point), log);
    }

    std::cout << "n evaluations: " << optimizer.n_evaluations() << '\n';
    std::cout << "n iterations: " << optimizer.n_iterations() << '\n';
    std::cout << "final point: " << optimizer.point() << '\n';

    if (!log_file_path.empty()) {
        json data = log;
        std::ofstream log_file(log_file_path);
        log_file << data.dump(4) << '\n';
    }

    if (!vis_file_path.empty()) {
        json data = ConjugateGradientDescentVis<-1>{log};
        std::ofstream vis_file(vis_file_path);
        vis_file << data.dump(4) << '\n';
    }

    return 0;
}
