#ifndef OPTIMIZATION_CONJUGATE_GRADIENT_DESCENT_LOGS_NOTHING_H
#define OPTIMIZATION_CONJUGATE_GRADIENT_DESCENT_LOGS_NOTHING_H

#include "utils/vector.h"

namespace optimization {

//--------------------------------------------------------------------------------------------------
struct ConjugateGradientDescentLogNothing {
    void reserve(uint32_t) {}
    template <typename Objective, int32_t N>
    void initialize(Objective const&, VectorNs<N> const&, Scalar, VectorNs<N> const&) {}
    template <int32_t N>
    void push_back(VectorNs<N> const&, VectorNs<N> const&, Scalar, VectorNs<N> const&) {}
    void clear() {}
};

}

#endif
