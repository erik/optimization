#ifndef OPTIMIZATION_CONJUGATE_GRADIENT_DESCENT_LOGS_EVERYTHING_VIS_H
#define OPTIMIZATION_CONJUGATE_GRADIENT_DESCENT_LOGS_EVERYTHING_VIS_H

#include "everything.h"

namespace optimization {

//--------------------------------------------------------------------------------------------------
template <int32_t N>
struct ConjugateGradientDescentVis {
    ConjugateGradientDescentLogEverything<N> const& log;
};

//..................................................................................................
template <int32_t N>
void to_json(nlohmann::json& j, ConjugateGradientDescentVis<N> const& vis) {
    j = nlohmann::json{
        {"algorithm", "conjugate gradient descent"},
        {"objective", vis.log.objective_name},
        {"data", {
            {"points", nlohmann::json::array()}
        }}
    };
    for (auto const& state : vis.log.states) {
        j["data"]["points"].push_back(state.point);
    }
}

}

#endif
