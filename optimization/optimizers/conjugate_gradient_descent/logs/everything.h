#ifndef OPTIMIZATION_CONJUGATE_GRADIENT_DESCENT_LOGS_EVERYTHING_H
#define OPTIMIZATION_CONJUGATE_GRADIENT_DESCENT_LOGS_EVERYTHING_H

#include "utils/eigen_json.h"
#include "utils/vector.h"
#include <string>
#include <vector>

namespace optimization {

//--------------------------------------------------------------------------------------------------
template <int32_t N>
struct ConjugateGradientDescentState {
    VectorNs<N> direction; // the direction we searched to get here (not where we will search next)
    VectorNs<N> point;     // where we are
    VectorNs<N> gradient;  // the gradient where we are
    Scalar value;          // the function value where we are
};

//--------------------------------------------------------------------------------------------------
template <int32_t N>
struct ConjugateGradientDescentLogEverything {
    void reserve(uint32_t n) { states.reserve(n); }
    template <typename Objective>
    void initialize(
        Objective const&,
        VectorNs<N> const& point,
        Scalar value,
        VectorNs<N> const& gradient
    );
    void push_back(
        VectorNs<N> const& direction,
        VectorNs<N> const& point,
        Scalar value,
        VectorNs<N> const& gradient
    );
    void clear();

    std::string objective_name;
    std::vector<ConjugateGradientDescentState<N>> states;
};

//..................................................................................................
template <int32_t N>
template <typename Objective>
void ConjugateGradientDescentLogEverything<N>::initialize(
    Objective const&,
    VectorNs<N> const& point,
    Scalar value,
    VectorNs<N> const& gradient
) {
    objective_name = Objective::name;
    states.emplace_back();
    // There is no initial direction.
    states.back().direction.resize(point.size());
    states.back().direction.setZero();
    states.back().point = point;
    states.back().gradient = gradient;
    states.back().value = value;
}

//..................................................................................................
template <int32_t N>
void ConjugateGradientDescentLogEverything<N>::push_back(
    VectorNs<N> const& direction,
    VectorNs<N> const& point,
    Scalar value,
    VectorNs<N> const& gradient
) {
    states.push_back({direction, point, gradient, value});
}

//..................................................................................................
template <int32_t N>
void ConjugateGradientDescentLogEverything<N>::clear() {
    objective_name.clear();
    states.clear();
}

//--------------------------------------------------------------------------------------------------
template <int32_t N>
void to_json(nlohmann::json& j, ConjugateGradientDescentState<N> const& state) {
    j = nlohmann::json{
        {"direction", state.direction},
        {"point", state.point},
        {"value", state.value},
        {"gradient", state.gradient}
    };
}

//--------------------------------------------------------------------------------------------------
template <int32_t N>
void to_json(nlohmann::json& j, ConjugateGradientDescentLogEverything<N> const& log) {
    j = nlohmann::json{
        {"algorithm", "conjugate gradient descent"},
        {"objective", log.objective_name},
        {"data", log.states}
    };
}

}

#endif
