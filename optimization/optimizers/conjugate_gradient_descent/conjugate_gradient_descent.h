#ifndef OPTIMIZATION_CONJUGATE_GRADIENT_DESCENT_H
#define OPTIMIZATION_CONJUGATE_GRADIENT_DESCENT_H

#include "logs/nothing.h"
#include "optimizers/line_search/brent.h"

#include <iostream>

namespace optimization {

//--------------------------------------------------------------------------------------------------
template <int32_t N>
class ConjugateGradientDescent {
public:
    using Vector = VectorNs<N>;

    ConjugateGradientDescent(
        Scalar gt = 1e-8,
        Scalar pt = 1e-8,
        uint32_t me = 10000,
        uint32_t mi = 1000
    ) :
        gradient_threshold_(gt),
        progress_threshold_(pt),
        max_evaluations_(me),
        max_iterations_(mi)
    {}

    uint32_t n_evaluations() const { return n_evaluations_; }
    uint32_t n_iterations() const { return n_iterations_; }
    VectorNs<N> const& point() const { return point_; }
    VectorNs<N> const& gradient() const { return gradient_; }
    Scalar value() const { return value_; }

    // There are four cases for optimize, since you can use a log or not, and can pass an rvalue or
    // not. These methods ultimately all call optimize_impl to do the real work.
    // Note: I explicitly write out the Vector const& and Vector&& cases rather than passing by
    // value and moving since Eigen forbids passing by value:
    // https://eigen.tuxfamily.org/dox/group__TopicPassingByValue.html
    template <typename Objective>
    Vector const& optimize(Objective& objective, Vector const& initial_point) {
        ConjugateGradientDescentLogNothing log;
        return optimize(objective, initial_point, log);
    }
    template <typename Objective, typename Log>
    Vector const& optimize(Objective& objective, Vector const& initial_point, Log& log) {
        point_ = initial_point;
        return optimize_impl(objective, log);
    }
    template <typename Objective>
    Vector const& optimize(Objective& objective, Vector&& initial_point) {
        ConjugateGradientDescentLogNothing log;
        return optimize(objective, std::move(initial_point), log);
    }
    template <typename Objective, typename Log>
    Vector const& optimize(Objective& objective, Vector&& initial_point, Log& log) {
        point_ = std::move(initial_point);
        return optimize_impl(objective, log);
    }

private:
    // Actually does the optimization, once point_ has been initialized.
    template <typename Objective, typename Log>
    Vector const& optimize_impl(Objective& objective, Log& log);

    // hyperparameters
    Scalar gradient_threshold_;
    Scalar progress_threshold_;
    uint32_t max_evaluations_;
    uint32_t max_iterations_;

    // algorithm state
    uint32_t n_evaluations_;
    uint32_t n_iterations_;
    Vector point_;         // where we are currently
    Vector direction_;     // the direction we're searching
    Vector gradient_;      // the gradient at point_
    // This variable sees double duty. When we're line searching, it holds the candidate points.
    // Between evaluating the function at the line minimum and computing the next search direction,
    // it stores the gradient at the point of the previous iteration.
    Vector new_point_or_last_gradient_;
    Scalar alpha_; // stores the most recent jump distance, which we use as a guess for the next one
    Scalar value_;
    Scalar last_value_;

    // algorithm constants
    static constexpr Scalar tiny_ = std::numeric_limits<Scalar>::epsilon();
};

//..................................................................................................
template <int32_t N>
template <typename Objective, typename Log>
VectorNs<N> const& ConjugateGradientDescent<N>::optimize_impl(
    Objective& objective,
    Log& log
) {
    n_evaluations_ = 0;
    n_iterations_ = 0;
    // point_ has already been initialized
    direction_.resize(point_.size());
    gradient_.resize(point_.size());
    new_point_or_last_gradient_.resize(point_.size());
    alpha_ = -1;

    auto const line_objective = [&](Scalar t, Scalar& value) {
        new_point_or_last_gradient_ = point_ + t * direction_;
        objective(new_point_or_last_gradient_, value);
    };
    BracketFinder bracket;
    Brent line_minimizer;

    objective(point_, value_, gradient_);
    ++n_evaluations_;
    direction_ = gradient_;

    log.initialize(objective, point_, value_, gradient_);

    // Return if the gradient is basically zero.
    if (gradient_.norm() < gradient_threshold_) {
        return point_;
    }

    while (true) {
        // Find the minimum along this direction. The next two lines are the only ones that use
        // new_point_or_last_gradient_ as new_point_ (happens inside the lambda line_objective).
        bracket.bracket(line_objective, Scalar(0), alpha_);
        alpha_ = line_minimizer.optimize(line_objective, bracket).point;
        n_evaluations_ += bracket.n_evaluations();
        n_evaluations_ += line_minimizer.n_evaluations();

        last_value_ = value_;
        // Basically this line means new_point_or_last_gradient_ = std::move(gradient_), but I want
        // to be clear that we're not releasing any memory here. For the rest of the loop,
        // new_point_or_last_gradient_ stores the last gradient.
        new_point_or_last_gradient_.swap(gradient_);
        // Again, we evaluted the objective here already during the line search, but we probably
        // threw that data out.
        // Note: at some point new_point_or_last_gradient_ already had this new value, but right now
        // there's no guarantee that it's the current value.
        point_ += alpha_ * direction_;
        objective(point_, value_, gradient_);
        ++n_iterations_;

        log.push_back(
            direction_,
            point_,
            value_,
            gradient_
        );

        // Check termination conditions.
        if (gradient_.norm() < gradient_threshold_) {
            std::cout << "reached gradient threshold (" << gradient_.norm() << ")\n";
            return point_;
        }
        if (2 * std::abs(last_value_ - value_) <=
            progress_threshold_ * (std::abs(last_value_) + std::abs(value_) + tiny_)
        ) {
            std::cout << "reached progress threshold\n";
            return point_;
        }
        if (n_evaluations_ > max_evaluations_) {
            std::cout << "reached max evaluations\n";
            return point_;
        }
        if (n_iterations_ > max_iterations_) {
            std::cout << "reached max iterations\n";
            return point_;
        }

        // Choose the next search direction. It is conjugate to all prior directions. Recall that
        // new_point_or_last_gradient_ currently stores the last gradient. I view this as the start
        // of the next iteration.
        Scalar const gamma = gradient_.dot(gradient_ - new_point_or_last_gradient_)
            / new_point_or_last_gradient_.dot(new_point_or_last_gradient_);
        direction_ = gradient_ + gamma * direction_;
    }
}

}

#endif
