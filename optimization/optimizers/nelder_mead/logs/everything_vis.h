#ifndef OPTIMIZATION_NELDER_MEAD_VIS_H
#define OPTIMIZATION_NELDER_MEAD_VIS_H

#include "everything.h"

namespace optimization {

//--------------------------------------------------------------------------------------------------
template <int32_t D>
struct NelderMeadVis {
    NelderMeadLogEverything<D> const& log;
};

//..................................................................................................
template <int32_t D>
void to_json(nlohmann::json& j, NelderMeadVis<D> const& vis) {
    j = nlohmann::json{
        {"algorithm", "nelder-mead"},
        {"objective", vis.log.objective_name},
        {"data", {
            {"polygons", nlohmann::json::array()}
        }}
    };
    for (auto const& state : vis.log.states) {
        j["data"]["polygons"].push_back(transpose_for_json(state.simplex));
    }
}

}

#endif
