#ifndef OPTIMIZATION_NELDER_MEAD_LOGS_EVERYTHING_H
#define OPTIMIZATION_NELDER_MEAD_LOGS_EVERYTHING_H

#include "json.hpp"
#include "../nelder_mead.h"
#include "utils/eigen_json.h"
#include <string>
#include <vector>

namespace optimization {

//--------------------------------------------------------------------------------------------------
template <int32_t D>
struct NelderMeadState {
    typename NelderMead<D>::MatrixDN simplex;
    typename NelderMead<D>::VectorN values;
};

//--------------------------------------------------------------------------------------------------
template <int32_t D>
struct NelderMeadLogEverything {
    template <typename Objective>
    void initialize(Objective const&) { objective_name = Objective::name; }

    void push_back(
        typename NelderMead<D>::MatrixDN const& simplex,
        typename NelderMead<D>::VectorN const& values
    ) {
        states.push_back({simplex, values});
    }

    void clear();

    std::string objective_name;
    std::vector<NelderMeadState<D>> states;
};

//..................................................................................................
template <int32_t D>
void NelderMeadLogEverything<D>::clear() {
    objective_name.clear();
    states.clear();
}

//--------------------------------------------------------------------------------------------------
template <int32_t D>
void to_json(nlohmann::json& j, NelderMeadState<D> const& state) {
    j = nlohmann::json{
        {"simplex", transpose_for_json(state.simplex)},
        {"values", state.values}
    };
}

//--------------------------------------------------------------------------------------------------
template <int32_t D>
void to_json(nlohmann::json& j, NelderMeadLogEverything<D> const& log) {
    j = nlohmann::json{
        {"algorithm", "gradient descent"},
        {"objective", log.objective_name},
        {"data", log.states}
    };
}

}

#endif
