#ifndef OPTIMIZATION_NELDER_MEAD_LOGS_NOTHING_H
#define OPTIMIZATION_NELDER_MEAD_LOGS_NOTHING_H

#include "utils/vector.h"
#include "utils/matrix.h"

namespace optimization {

//--------------------------------------------------------------------------------------------------
struct NelderMeadLogNothing {
    template <typename Objective>
    void initialize(Objective const&) {}

    template <int32_t D, int32_t N>
    void push_back(MatrixNs<D, N> const&, VectorNs<N> const&) {}
};

}

#endif
