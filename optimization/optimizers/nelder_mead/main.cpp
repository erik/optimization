#include "clara.hpp"
#include "logs/everything_vis.h"
#include "nelder_mead.h"
#include "objectives/paraboloid.h"
#include "objectives/rosenbrock.h"
#include <iostream>
#include <fstream>

using namespace optimization;
using json = nlohmann::json;

//--------------------------------------------------------------------------------------------------
int main(int const argc, char const** argv) {
    std::string log_file_path;
    std::string vis_file_path;
    uint32_t dim = 2;
    uint32_t max_evaluations = 1000;
    Scalar relative_y_tolerance = 1e-8;

    auto const cli =
        clara::Arg(log_file_path, "log_file_path")("Location of the optimization log") |
        clara::Arg(vis_file_path, "vis_file_path")("Location of the visualization file") |
        clara::Opt(dim, "dim")["-d"]["--dim"]("Dimension of the search space") |
        clara::Opt(max_evaluations, "max_evaluations")["-n"]["--max-evals"]("Max number of function evaluations") |
        clara::Opt(relative_y_tolerance, "relative_y_tolerance")["-y"]["--rel-y-tol"]("Relative tolerance for function values (termination condition)");
    auto const result = cli.parse(clara::Args(argc, argv));
    if (!result) {
        std::cerr << "Error in command line: " << result.errorMessage() << std::endl;
        exit(1);
    }

    MatrixXs simplex;
    simplex.resize(dim, dim + 1);
    simplex.fill(-1);
    simplex.row(1).fill(2);
    for (uint32_t i = 0; i < dim; ++i) {
        simplex(i, i + 1) -= 1;
    }

    //using Objective = Paraboloid<Vector2<Scalar>>;
    using Objective = Rosenbrock<-1>;
    Objective objective;

    NelderMead<-1> optimizer(max_evaluations, relative_y_tolerance);
    NelderMeadLogEverything<-1> log;

    // Only log stuff if we're going to use it.
    if (log_file_path.empty() && vis_file_path.empty()) {
        optimizer.optimize(objective, std::move(simplex));
    } else {
        optimizer.optimize(objective, std::move(simplex), log);
    }

    std::cout << "n evaluations: " << optimizer.n_evaluations() << '\n';
    std::cout << "final point: " << optimizer.point() << '\n';

    if (!log_file_path.empty()) {
        json data = log;
        std::ofstream log_file(log_file_path);
        log_file << data.dump(4) << '\n';
    }

    if (!vis_file_path.empty()) {
        json data = NelderMeadVis<-1>{log};
        std::ofstream vis_file(vis_file_path);
        vis_file << data.dump(4) << '\n';
    }

    return 0;
}
