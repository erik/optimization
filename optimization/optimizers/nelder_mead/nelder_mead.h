#ifndef OPTIMIZATION_NELDER_MEAD_H
#define OPTIMIZATION_NELDER_MEAD_H

#include "logs/nothing.h"
#include <cmath>
#include <iostream>

namespace optimization {

//--------------------------------------------------------------------------------------------------
// TODO record termination condition
template <int32_t D = -1>
class NelderMead {
public:
    // If D != -1, N == D + 1. If D == -1, N == -1.
    static constexpr int32_t N = [D_ = D](){
        if (D_ < 0) {
            return D_;
        } else {
            return D_ + 1;
        }
    }();

    // vector in the space we're searching
    using VectorD = VectorNs<D>;
    // vector in one higher dimension (e.g. to store a real value for each simplex vertex)
    using VectorN = VectorNs<N>;
    // D x N matrix (each column is a VectorD holding a vertex, each row is a VectorN)
    // There are two reasons to store vertices as columns. First, it avoids the use of
    // transposition. Eigen will happily tranpose for you if you assign a row vector to a column
    // vector, but it will complain if you do something like `c_vec = c_vec2 + r_vec`. Second, Eigen
    // defaults to column major storage, so this keeps our vertex values contiguous. For logging we
    // transpose the matrix, so each row is a vertex, since this is more natural to deal with in
    // numpy/matplotlib.
    using MatrixDN = MatrixNs<D, N>;

    // Constructor
    NelderMead(uint32_t me = 1000, Scalar ry = 1e-8)
        : max_evaluations_(me), relative_y_tolerance_(ry)
    {}

    uint32_t n_evaluations() const { return n_evaluations_; }
    MatrixDN const& simplex_vertices() const { return simplex_vertices_; }
    VectorN const& simplex_values() const { return simplex_values_; }
    //VectorD const& min_point() const { return simplex_vertices_[i_lowest_]; }
    decltype(auto) point() const {
        return simplex_vertices_.col(i_lowest_);
    }
    Scalar value() const { return simplex_values_[i_lowest_]; }

    // All optimization methods return the best point, as a reference to a column (like point()).
    // These two construct a simplex from an initial point by offsetting all coordinate values.
    template <typename Objective>
    decltype(auto) optimize(
        Objective& objective,
        VectorD const& initial_point,
        Scalar offset = 1
    ) {
        NelderMeadLogNothing log;
        return optimize(objective, initial_point, offset, log);
    }
    template <typename Objective, typename Log>
    decltype(auto) optimize(
        Objective& objective,
        VectorD const& initial_point,
        Scalar offset,
        Log& log
    );
    // These four take a full simplex. Each column is a vertex.
    template <typename Objective>
    decltype(auto) optimize(Objective& objective, MatrixDN&& simplex) {
        NelderMeadLogNothing log;
        return optimize(objective, std::move(simplex), log);
    }
    template <typename Objective, typename Log>
    decltype(auto) optimize(Objective& objective, MatrixDN&& simplex, Log& log) {
        simplex_vertices_ = std::move(simplex);
        return optimize_impl(objective, log);
    }
    template <typename Objective>
    decltype(auto) optimize(Objective& objective, MatrixDN const& simplex) {
        NelderMeadLogNothing log;
        return optimize(objective, simplex, log);
    }
    template <typename Objective, typename Log>
    decltype(auto) optimize(Objective& objective, MatrixDN const& simplex, Log& log) {
        simplex_vertices_ = simplex;
        return optimize_impl(objective, log);
    }

private:
    // Does the optimization assuming simplex_vertices_ has been initialized.
    template <typename Objective, typename Log>
    decltype(auto) optimize_impl(Objective& objective, Log& log);

    // Helper method for optimize_impl.
    template <typename Objective>
    Scalar try_new_point(Objective& objective, Scalar factor);

    // hyperparameters
    uint32_t max_evaluations_;
    //Scalar relative_x_tolerance_;
    Scalar relative_y_tolerance_;

    // algorithm state
    // For fixed size D (and thus N), dim_ and n_vertices_ are redundant.
    uint32_t dim_;              // we are searching in R^dim_
    uint32_t n_vertices_;       // the simplex has n_vertices_ == dim_ + 1 different vertices
    uint32_t n_evaluations_;    // number of times we've evaluated the objective
    MatrixDN simplex_vertices_; // the simplex itself (each column is a vertex)
    VectorN simplex_values_;    // the function values at the vertices
    VectorD vertex_sum_;        // the sum of all the vertices
    VectorD x_new_;             // used for trial points
    // Invariant:
    // simplex_values_[i_lowest] <= simplex_values_[i_second_highest] <= simplex_values_[i_highest]
    uint32_t i_lowest_;
    uint32_t i_second_highest_;
    uint32_t i_highest_;

    // algorithm constants
    static constexpr Scalar reflection_coefficient_ = -1.0;
    static constexpr Scalar expansion_coefficient_ = 2.0;
    static constexpr Scalar contraction_coefficient_ = 0.5;
    static constexpr Scalar shrinking_coefficient_ = 0.5;
    static constexpr Scalar tiny_ = 1e-10;
};

//..................................................................................................
template <int32_t D>
template <typename Objective, typename Log>
decltype(auto) NelderMead<D>::optimize(
    Objective& objective,
    VectorD const& initial_point,
    Scalar offset,
    Log& log
) {
    simplex_vertices_.resize(initial_point.size(), initial_point.size() + 1);
    simplex_vertices_.col(0) = initial_point;
    for (uint32_t i = 0; i < initial_point.size(); ++i) {
        simplex_vertices_.col(i + 1) = initial_point;
        simplex_vertices_(i, i + 1) += offset;
    }
    return optimize_impl(objective, log);
}

//..................................................................................................
template <int32_t D>
template <typename Objective, typename Log>
decltype(auto) NelderMead<D>::optimize_impl(Objective& objective, Log& log) {
    // initialize state, assuming simplex_vertices_ is already set
    n_vertices_ = simplex_vertices_.cols();
    dim_ = simplex_vertices_.rows();
    assert(n_vertices_ == dim_ + 1);
    simplex_values_.resize(n_vertices_);
    vertex_sum_ = simplex_vertices_.rowwise().sum();
    log.initialize(objective);

    // Evaluate the objective at all simplex vertices.
    for (uint32_t i = 0u; i < n_vertices_; ++i) {
        objective(simplex_vertices_.col(i), simplex_values_[i]);
    }
    n_evaluations_ = n_vertices_;

    while (true) {
        // Find lowest, highest, and second highest points.
        i_lowest_ = (simplex_values_[0] < simplex_values_[1]) ? 0 : 1;
        i_second_highest_ = i_lowest_;
        i_highest_ = 1 - i_lowest_;
        for (uint32_t i = 2; i < n_vertices_; ++i) {
            Scalar const y = simplex_values_[i];
            if (y < simplex_values_[i_lowest_]) {
                i_lowest_ = i;
            }
            else if (y > simplex_values_[i_highest_]) {
                i_second_highest_ = i_highest_;
                i_highest_ = i;
            }
            else if (y > simplex_values_[i_second_highest_]) {
                i_second_highest_ = i;
            }
        }

        // Log the simplex.
        log.push_back(simplex_vertices_, simplex_values_);

        // Evaluate termination conditions.
        Scalar const difference = std::abs(simplex_values_[i_highest_] - simplex_values_[i_lowest_]);
        Scalar const sum = std::abs(simplex_values_[i_highest_] + simplex_values_[i_lowest_]);
        Scalar const relative_y_difference = 2 * difference / (sum + tiny_);
        if (relative_y_difference < relative_y_tolerance_) {
            std::cout << "Achieved relative tolerance of y: " << relative_y_difference << '\n';
            return simplex_vertices_.col(i_lowest_);
        }
        if (n_evaluations_ > max_evaluations_) {
            std::cout << "Exceeded max function evaluations.\n";
            return simplex_vertices_.col(i_lowest_);
        }

        // Try reflecting the worst point.
        Scalar const y_1 = try_new_point(objective, reflection_coefficient_);

        // If the new point is the best so far, try going further in the same direction (expansion).
        if (y_1 <= simplex_values_[i_lowest_]) {
            try_new_point(objective, expansion_coefficient_);
            continue;
        }

        // If it wasn't the best, but it still makes a different point the worst, keep it.
        if (y_1 < simplex_values_[i_second_highest_]) {
            continue;
        }

        // If the reflected point is still the worst, try contracting along one dimension.
        // Note that we could be contracting from the original point, or the reflected point.
        Scalar const y_hi = simplex_values_[i_highest_];
        Scalar const y_2 = try_new_point(objective, contraction_coefficient_);

        // If the contracted point is better than the worst, keep it.
        if (y_2 < y_hi) {
            continue;
        }

        // If the contracted point is even worse, shrink everything.
        for (uint32_t i = 0; i < n_vertices_; ++i) {
            if (i != i_lowest_) {
                simplex_vertices_.col(i) =
                    shrinking_coefficient_ * (simplex_vertices_.col(i_lowest_) + simplex_vertices_.col(i));
                objective(simplex_vertices_.col(i), simplex_values_[i]);
            }
        }
        n_evaluations_ += dim_;
        // The vertex sum must be recomputed from scratch in this case.
        vertex_sum_ = simplex_vertices_.rowwise().sum();
    }
}

//..................................................................................................
template <int32_t D>
template <typename Objective>
Scalar NelderMead<D>::try_new_point(Objective& objective, Scalar factor) {
    // Generate a new point by reflecting/expanding/contracting the worst point.
    Scalar const t1 = (Scalar(1) - factor) / dim_;
    Scalar const t2 = factor - t1;
    VectorD const x_new = t1 * vertex_sum_ + t2 * simplex_vertices_.col(i_highest_);

    // Evaluate the new point.
    Scalar y_new;
    objective(x_new, y_new);
    ++n_evaluations_;

    // If the new point is an improvement, keep it.
    if (y_new < simplex_values_[i_highest_]) {
        vertex_sum_ += x_new - simplex_vertices_.col(i_highest_);
        simplex_vertices_.col(i_highest_) = x_new;
        simplex_values_[i_highest_] = y_new;
    }
    return y_new;
}

}

#endif
