#include "clara.hpp"
#include "cma_es.h"
#include "objectives/paraboloid.h"
#include "objectives/rosenbrock.h"
#include "logs/everything_vis.h"
#include <fstream>
#include <iostream>

using namespace optimization;
using json = nlohmann::json;

//--------------------------------------------------------------------------------------------------
int main(int const argc, char const** argv) {
    std::string log_file_path;
    std::string vis_file_path;
    uint32_t dim = 2;
    uint32_t pop_size = 10;
    int32_t seed = 0xdefceed9;
    uint32_t max_evaluations = 1000;
    uint32_t max_iterations = 1000;
    Scalar rel_y_tol = 1e-8;
    Scalar x0 = -1;
    Scalar y0 = 2;
    Scalar std_dev = 1;

    auto const cli =
        clara::Arg(log_file_path, "log_file_path")("Location of the optimization log") |
        clara::Arg(vis_file_path, "vis_file_path")("Location of the visualization file") |
        clara::Opt(dim, "dim")["-d"]["--dimension"]("Dimensionality of the objective") |
        clara::Opt(pop_size, "pop_size")["-p"]["--pop-size"]("Population size") |
        clara::Opt(seed, "seed")["-s"]["--seed"]("Seed for CMA-ES random number generator") |
        clara::Opt(max_evaluations, "max_evaluations")["-n"]["--max-evaluations"]("Max number of function evaluations") |
        clara::Opt(max_iterations, "max_iterations")["-i"]["--max-iterations"]("Max number of generations") |
        clara::Opt(rel_y_tol, "rel_y_tol")["-t"]["--rel-y-tol"]("Termination condition") |
        clara::Opt(x0, "x0")["-x"]["--x0"]("X coordinate of initial point") |
        clara::Opt(y0, "y0")["-y"]["--y0"]("Y coordinate of initial point");
        clara::Opt(std_dev, "std_dev")["-d"]["--std-dev"]("Initial standard deviation");
    auto const result = cli.parse(clara::Args(argc, argv));
    if (!result) {
        std::cerr << "Error in command line: " << result.errorMessage() << std::endl;
        exit(1);
    }

    VectorXs initial_point;
    initial_point.resize(dim);
    initial_point[0] = x0;
    initial_point[1] = y0;
    for (uint32_t i = 2; i < dim; ++i) {
        initial_point[i] = -1;
    }

    VectorXs initial_std_dev;
    initial_std_dev.resize(dim);
    initial_std_dev.fill(std_dev);

    //using Objective = Paraboloid<Vector2<Scalar>>;
    using Objective = Rosenbrock<-1>;
    Objective objective;

    CmaEs optimizer(dim, pop_size, seed, max_evaluations, max_iterations, rel_y_tol);
    CmaEsLogEverything log;

    if (log_file_path.empty() && vis_file_path.empty()) {
        optimizer.optimize(objective, initial_point, initial_std_dev);
    } else {
        optimizer.optimize(objective, initial_point, initial_std_dev, log);
    }

    std::cout << "n evaluations: " << optimizer.n_evaluations() << '\n';
    std::cout << "n generations: " << optimizer.n_iterations() << '\n';
    std::cout << "final point: " << optimizer.point() << '\n';

    if (!log_file_path.empty()) {
        json data = log;
        std::ofstream log_file(log_file_path);
        log_file << data.dump(4) << '\n';
    }

    if (!vis_file_path.empty()) {
        json data = CmaEsVis{log};
        std::ofstream vis_file(vis_file_path);
        vis_file << data.dump(4) << '\n';
    }

    return 0;
}
