#ifndef OPTIMIZATION_CMA_ES_H
#define OPTIMIZATION_CMA_ES_H

#include "cmaes_interface.h"
#include "logs/nothing.h"
#include "utils/vector.h"
#include <iostream>

namespace optimization {

//--------------------------------------------------------------------------------------------------
class CmaEs {
public:
    CmaEs(): points_(nullptr), values_(nullptr) {}
    CmaEs(uint32_t d, uint32_t ps, int32_t s, uint32_t me, uint32_t mi, Scalar ryt) :
        dim_(d),
        pop_size_(ps),
        seed_(s),
        max_evaulations_(me),
        max_iterations_(mi),
        rel_y_tol_(ryt),
        points_(nullptr),
        values_(nullptr)
    {}
    ~CmaEs() { if (points_ != nullptr) { cmaes_exit(&cma_es_); } }

    uint32_t dim() const { return dim_; }
    uint32_t& dim() { return dim_; }
    uint32_t pop_size() const { return pop_size_; }
    uint32_t& pop_size() { return pop_size_; }

    uint32_t n_evaluations() { return cmaes_Get(&cma_es_, "eval"); }
    uint32_t n_iterations() const { return n_iterations_; }
    // TODO See if I can make these functions const by modifying some CMA-ES guts.
    inline Eigen::Map<const Eigen::MatrixXd> point();
    Scalar value() { return cmaes_Get(&cma_es_, "fbestever"); }

    // TODO: Support passing initial point by rvalue reference.
    template <typename Objective>
    Eigen::Map<const Eigen::MatrixXd> optimize(
        Objective& objective,
        VectorXs const& initial_point,
        Scalar initial_std_dev = 0.3
    );
    template <typename Objective, typename Log>
    Eigen::Map<const Eigen::MatrixXd> optimize(
        Objective& objective,
        VectorXs const& initial_point,
        Scalar initial_std_dev,
        Log& log
    );

    template <typename Objective>
    Eigen::Map<const Eigen::MatrixXd> optimize(
        Objective& objective,
        VectorXs const& initial_point,
        VectorXs const& initial_std_dev
    );
    template <typename Objective, typename Log>
    Eigen::Map<const Eigen::MatrixXd> optimize(
        Objective& objective,
        VectorXs const& initial_point,
        VectorXs const& initial_std_dev,
        Log& log
    );

private:
    // hyperparameters
    uint32_t dim_;
    uint32_t pop_size_;
    int32_t seed_;
    uint32_t max_evaulations_;
    uint32_t max_iterations_;
    Scalar rel_y_tol_;

    // algorithm state
    uint32_t n_iterations_;
    cmaes_t cma_es_;
    // These are used to communicate with the CMA-ES object. They also help with memory management,
    // in that cma_es_ has memory that eventually needs to be freed if and only if points_ and
    // values_ are nullptr.
    double* const* points_; // points to cma_es_'s population points when we're optimizing
    double* values_;        // points to cma_es_'s population values when we're optimizing
};

//..................................................................................................
Eigen::Map<const Eigen::MatrixXd> CmaEs::point() {
    double const* best_point = cmaes_GetPtr(&cma_es_, "xbestever");
    return Eigen::Map<const Eigen::MatrixXd>(best_point, dim_, 1);
}

//..................................................................................................
template <typename Objective>
Eigen::Map<const Eigen::MatrixXd> CmaEs::optimize(
    Objective& objective,
    VectorXs const& initial_point,
    Scalar initial_std_dev
) {
    CmaEsLogNothing log;
    return optimize(objective, initial_point, initial_std_dev, log);
}

//..................................................................................................
template <typename Objective, typename Log>
Eigen::Map<const Eigen::MatrixXd> CmaEs::optimize(
    Objective& objective,
    VectorXs const& initial_point,
    Scalar initial_std_dev,
    Log& log
) {
    VectorXs std_dev_vector;
    std_dev_vector.resize(initial_point.size());
    std_dev_vector.fill(initial_std_dev);
    return optimize(objective, initial_point, std_dev_vector, log);
}

//..................................................................................................
template <typename Objective>
Eigen::Map<const Eigen::MatrixXd> CmaEs::optimize(
    Objective& objective,
    VectorXs const& initial_point,
    VectorXs const& initial_std_dev
) {
    CmaEsLogNothing log;
    return optimize(objective, initial_point, initial_std_dev, log);
}

//..................................................................................................
template <typename Objective, typename Log>
Eigen::Map<const Eigen::MatrixXd> CmaEs::optimize(
    Objective& objective,
    VectorXs const& initial_point,
    VectorXs const& initial_std_dev,
    Log& log
) {
    n_iterations_ = 0;
    if (points_ != nullptr) {
        cmaes_exit(&cma_es_);
    }

    // This is used for various copy operations. Shouldn't be necessary.
    VectorXs point_vec = initial_point;
    {
        // For some reason cmaes_init_para takes double*, not double const*, so I have to copy these.
        // TODO: Looking at Hansen's code, there's no reason they aren't const. I should submit a PR.
        VectorXs std_dev = initial_std_dev;

        cmaes_init_para(
            &cma_es_,
            dim_,
            point_vec.data(),
            std_dev.data(),
            seed_,
            pop_size_,
            "none"
        );
    }
    cma_es_.sp.stopMaxFunEvals = max_evaulations_;
    cma_es_.sp.stopMaxIter = max_iterations_;
    cma_es_.sp.stopTolFun = rel_y_tol_;
    values_ = cmaes_init_final(&cma_es_);

    log.initialize(objective);
    std::cout << cmaes_SayHello(&cma_es_) << '\n';

    while (!cmaes_TestForTermination(&cma_es_)) {
        // Get a generation of points.
        points_ = cmaes_SamplePopulation(&cma_es_);

        // TODO: This wants to be parallelized.
        for (uint32_t i = 0; i < pop_size_; ++i) {
            auto point_map = Eigen::Map<const Eigen::MatrixXd>(points_[i], dim_, 1);
            // TODO: make objectives handle Maps so we don't have to copy the data.
            point_vec = point_map;
            objective(point_vec, values_[i]);
        }

        log.push_back(points_, values_, dim_, pop_size_);

        // Update the search distribution used for cmaes_SamplePopulation().
        cmaes_UpdateDistribution(&cma_es_, values_);
        ++n_iterations_;
    }

    std::cout << cmaes_TestForTermination(&cma_es_) << '\n';

    // Get best estimator for the optimum, xmean.
    return point();
}

}

#endif
