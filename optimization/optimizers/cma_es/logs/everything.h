#ifndef OPTIMIZATION_CMA_ES_LOGS_EVERYTHING_H
#define OPTIMIZATION_CMA_ES_LOGS_EVERYTHING_H

#include "utils/matrix.h"
#include "utils/vector.h"
#include "utils/eigen_json.h"
#include <vector>

namespace optimization {

//--------------------------------------------------------------------------------------------------
struct CmaEsState {
    // each column is a point
    MatrixXs points;
    // each column of points has a corresponding entry in values
    VectorXs values;
};

//--------------------------------------------------------------------------------------------------
struct CmaEsLogEverything {
    inline void reserve(uint32_t n) { states.reserve(n); }
    template <typename Objective>
    inline void initialize(Objective const&) { objective_name = Objective::name; }
    inline void push_back(
        double const* const* points,
        double const* values,
        uint32_t dim,
        uint32_t pop_size
    );
    inline void clear();

    std::string objective_name;
    std::vector<CmaEsState> states;
};

//..................................................................................................
void CmaEsLogEverything::push_back(
    double const* const* points,
    double const* values,
    uint32_t dim,
    uint32_t pop_size
) {
    states.emplace_back();
    states.back().points.resize(dim, pop_size);
    states.back().values.resize(pop_size);
    for (uint32_t i = 0; i < pop_size; ++i) {
        for (uint32_t j = 0; j < dim; ++j) {
            states.back().points(j, i) = points[i][j];
        }
        states.back().values[i] = values[i];
    }
}

//..................................................................................................
void CmaEsLogEverything::clear() {
    objective_name.clear();
    states.clear();
}

//--------------------------------------------------------------------------------------------------
inline void to_json(nlohmann::json& j, CmaEsState const& state) {
    j = nlohmann::json{
        {"points", transpose_for_json(state.points)},
        {"values", state.values}
    };
}

//--------------------------------------------------------------------------------------------------
inline void to_json(nlohmann::json& j, CmaEsLogEverything const& log) {
    j = nlohmann::json{
        {"algorithm", "gradient descent"},
        {"objective", log.objective_name},
        {"data", log.states}
    };
}

}

#endif
