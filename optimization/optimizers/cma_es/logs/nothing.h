#ifndef OPTIMIZATION_CMA_ES_LOGS_NOTHING_H
#define OPTIMIZATION_CMA_ES_LOGS_NOTHING_H

#include <cstdint>

namespace optimization {

//--------------------------------------------------------------------------------------------------
struct CmaEsLogNothing {
    inline void reserve(uint32_t) {}
    template <typename Objective>
    inline void initialize(Objective const&) {}
    inline void push_back(double const* const*, double const*, uint32_t, uint32_t) {}
    inline void clear() {}
};

}

#endif
