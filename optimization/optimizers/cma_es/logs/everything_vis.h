#ifndef OPTIMIZATION_CMA_ES_LOGS_EVERYTHING_VIS_H
#define OPTIMIZATION_CMA_ES_LOGS_EVERYTHING_VIS_H

#include "everything.h"

namespace optimization {

//--------------------------------------------------------------------------------------------------
struct CmaEsVis {
    CmaEsLogEverything const& log;
};

//..................................................................................................
inline void to_json(nlohmann::json& j, CmaEsVis const& vis) {
    j = nlohmann::json{
        {"algorithm", "CMA-ES"},
        {"objective", vis.log.objective_name},
        {"data", {
            {"points", nlohmann::json::array()}
        }}
    };
    for (auto const& state : vis.log.states) {
        for (uint32_t i = 0; i < state.points.cols(); ++i) {
            // Hack: just want to filter these so the CMA-ES plot is the same size as the others.
            // Obviously bad design to have 2.5 hard coded here and the plot script...
            if (
                state.points(0, i) >= -2.5 &&
                state.points(0, i) <= 2.5 &&
                state.points(1, i) >= -2.5 &&
                state.points(1, i) <= 2.5
            ) {
                auto point = nlohmann::json::array();
                for (uint32_t j = 0; j < state.points.rows(); ++j) {
                    point.push_back(state.points(j, i));
                }
                j["data"]["points"].push_back(point);
            }
        }
    }
}

}

#endif
