#ifndef OPTIMIZATION_OPTIMIZERS_LINE_SEARCH_BRACKET_H
#define OPTIMIZATION_OPTIMIZERS_LINE_SEARCH_BRACKET_H

#include "utils/scalar.h"
#include <utility> // std::swap

namespace optimization {

//--------------------------------------------------------------------------------------------------
// TODO: Could be nice for this class to guarantee y_1_ > y_2_ < y_3_ and x_1_ < x_2_ < x_3_ or
// x_1_ > x_2_ > x_3_ (with the "or" evaluted before the "and"). But for now this feels like
// egregious overengineering.
class Bracket {
public:
    Bracket() {}
    Bracket(Scalar x_1, Scalar x_2, Scalar x_3, Scalar y_1, Scalar y_2, Scalar y_3)
        : x_1_(x_1), x_2_(x_2), x_3_(x_3), y_1_(y_1), y_2_(y_2), y_3_(y_3)
    {}
    Bracket(Bracket const&) = default;
    Bracket& operator=(Bracket const&) = default;

    Scalar x_1() const { return x_1_; }
    Scalar x_2() const { return x_2_; }
    Scalar x_3() const { return x_3_; }
    Scalar y_1() const { return y_1_; }
    Scalar y_2() const { return y_2_; }
    Scalar y_3() const { return y_3_; }

protected:
    Scalar x_1_;
    Scalar x_2_;
    Scalar x_3_;
    Scalar y_1_;
    Scalar y_2_;
    Scalar y_3_;
};

//--------------------------------------------------------------------------------------------------
// TODO: Should this have a failsafe max number of function evals (for monotonic objectives)?
class BracketFinder : public Bracket {
public:
    BracketFinder(): n_evaluations_(0) {}

    uint32_t n_evaluations() const { return n_evaluations_; }

    template <typename Objective>
    void bracket(Objective& objective, Scalar x_1, Scalar x_2);

private:
    uint32_t n_evaluations_;

    static constexpr Scalar golden_ratio_ = 1.618034;
    static constexpr Scalar max_ratio_ = 1e2;
    static constexpr Scalar tiny_ = 1e-10;
};

//..................................................................................................
// TODO: How should this method handle flat functions?
template <typename Objective>
void BracketFinder::bracket(Objective& objective, Scalar x_1, Scalar x_2) {
    // Copy in starting values and perform first evaluations.
    x_1_ = x_1;
    x_2_ = x_2;
    objective(x_1_, y_1_);
    objective(x_2_, y_2_);
    n_evaluations_ = 2;

    // Ensure that y_2_ < y_1_.
    assert(y_1_ != y_2_);
    if (y_1_ < y_2_) {
        std::swap(x_1_, x_2_);
        std::swap(y_1_, y_2_);
    }

    // First try a golden ratio step.
    // From here on out, either x_1_ < x_2_ < x_3_ or x_1_ > x_2_ > x_3_.
    x_3_ = x_2_ + golden_ratio_ * (x_2_ - x_1_);
    objective(x_3_, y_3_);
    ++n_evaluations_;

    // Search until we have a bracket.
    // Inside this loop, y_1_ > y_2_ > y_3_. So we don't know if x_1_ -> x_2_ -> x_3_ is going left
    // or right, but we do know it's going downhill.
    while (y_2_ > y_3_) {
        // Use parabolic interpolation to guess the minimum.
        // This formula can be derived by constructing the relevant Lagrange polynomials, and
        // setting the derivative to zero. This is not the most symmetric form of the answer, but it
        // involves no squaring.
        Scalar const tmp_1 = (x_2_ - x_3_) * (y_2_ - y_1_);
        Scalar const tmp_2 = (x_2_ - x_1_) * (y_2_ - y_3_);
        Scalar const tmp_3 = tmp_1 - tmp_2;
        Scalar const sign = (tmp_3 >= 0) ? 1 : -1;
        Scalar const numerator = sign * (tmp_1 * (x_2_ - x_3_) - tmp_2 * (x_2_ - x_1_));
        Scalar const denominator = 2 * std::max(sign * tmp_3, tiny_);
        Scalar x_new = x_2_ - numerator / denominator;
        Scalar y_new;

        // Don't step past this point.
        Scalar const x_lim = x_2_ + max_ratio_ * (x_3_ - x_2_);

        if ((x_new - x_2_) * (x_3_ - x_new) > Scalar(0)) {
            // If the new point between x_2_ and x_3_, try it.
            objective(x_new, y_new);
            ++n_evaluations_;

            if (y_new < y_3_) {
                // x_2_, x_new, x_3_ is a bracket
                x_1_ = x_2_;
                x_2_ = x_new;
                y_1_ = y_2_;
                y_2_ = y_new;
                return;
            } else if (y_2_ < y_new) {
                // x_1_, x_2_, x_new is a bracket
                x_3_ = x_new;
                y_3_ = y_new;
                return;
            }
        } else if ((x_new - x_3_) * (x_lim - x_new) > Scalar(0)) {
            // If the new point seems to be downhill and is not super far away, try it.
            objective(x_new, y_new);
            ++n_evaluations_;

            if (y_new < y_3_) {
                // We're still going downhill. Get rid of x_2_ and try a golden ratio step.
                Scalar const step = golden_ratio_ * (x_new - x_3_);
                x_2_ = x_3_;
                x_3_ = x_new;
                x_new += step;
                y_2_ = y_3_;
                y_3_ = y_new;
                objective(x_new, y_new);
                ++n_evaluations_;
            }
        } else if ((x_lim - x_3_) * (x_new - x_lim) >= Scalar(0)) {
            // If the new point is past x_lim, try x_lim next.
            x_new = x_lim;
            objective(x_new, y_new);
            ++n_evaluations_;
        } else {
            // If the new point seems to be too far uphill, just do a golden ratio step.
            x_new = x_3_ + golden_ratio_ * (x_3_ - x_2_);
            objective(x_new, y_new);
            ++n_evaluations_;
        }

        // Get rid of the highest point.
        x_1_ = x_2_;
        x_2_ = x_3_;
        x_3_ = x_new;
        y_1_ = y_2_;
        y_2_ = y_3_;
        y_3_ = y_new;
    }
}

}

#endif
