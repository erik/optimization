#ifndef OPTIMIZATION_UTILS_VECTOR_H
#define OPTIMIZATION_UTILS_VECTOR_H

#include "scalar.h"
#include <Eigen/Core>

namespace optimization {

//--------------------------------------------------------------------------------------------------
template <typename T>
using Vector2 = Eigen::Matrix<T, 2, 1>;

//--------------------------------------------------------------------------------------------------
template <typename T>
using Vector3 = Eigen::Matrix<T, 3, 1>;

//--------------------------------------------------------------------------------------------------
template <typename T>
using VectorX = Eigen::Matrix<T, Eigen::Dynamic, 1>;

//--------------------------------------------------------------------------------------------------
using Vector2s = Vector2<Scalar>;
using Vector3s = Vector3<Scalar>;
using VectorXs = VectorX<Scalar>;

//--------------------------------------------------------------------------------------------------
template <int32_t N>
using VectorNs = Eigen::Matrix<Scalar, N, 1>;

}

#endif
