#ifndef OPTIMIZATION_UTILS_MATRIX_H
#define OPTIMIZATION_UTILS_MATRIX_H

#include "scalar.h"
#include <Eigen/Core>

namespace optimization {

//--------------------------------------------------------------------------------------------------
template <typename T>
using Matrix2 = Eigen::Matrix<T, 2, 2>;

//--------------------------------------------------------------------------------------------------
template <typename T>
using Matrix3 = Eigen::Matrix<T, 3, 3>;

//--------------------------------------------------------------------------------------------------
template <typename T>
using MatrixX = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>;

//--------------------------------------------------------------------------------------------------
using Matrix2s = Matrix2<Scalar>;
using Matrix3s = Matrix3<Scalar>;
using MatrixXs = MatrixX<Scalar>;

//--------------------------------------------------------------------------------------------------
template <int32_t N, int32_t M>
using MatrixNs = Eigen::Matrix<Scalar, N, M>;

}

#endif
