#ifndef OPTIMIZATION_UTILS_EIGEN_JSON_H
#define OPTIMIZATION_UTILS_EIGEN_JSON_H

#include "json.hpp"
#include <Eigen/Core>

namespace optimization {

//--------------------------------------------------------------------------------------------------
// serialization of matrix transposes
// I can't get this to work with Eigen's crazy templates inside nlohmann so I'm doing it myself.
template <typename T, int32_t N1, int32_t N2>
struct EigenMatrixTranspose {
    Eigen::Matrix<T, N1, N2> const& matrix;
};

//--------------------------------------------------------------------------------------------------
template <typename T, int32_t N1, int32_t N2>
EigenMatrixTranspose<T, N1, N2> transpose_for_json(Eigen::Matrix<T, N1, N2> const& matrix) {
    return EigenMatrixTranspose<T, N1, N2>{matrix};
}

}

namespace nlohmann {

//--------------------------------------------------------------------------------------------------
// serialization of vectors
// Note: for some reason nlohmann::json can't figure out what's going on if I try to use DenseBase.
template <typename T, int32_t N>
struct adl_serializer<Eigen::Matrix<T, N, 1>> {
    static void to_json(json& j, Eigen::Matrix<T, N, 1> const& vector) {
        for (uint32_t i = 0; i < vector.size(); ++i) {
            j.push_back(vector[i]);
        }
    }

    static void from_json(json const& j, Eigen::Matrix<T, N, 1>& vector) {
        // TODO check size of vector (have to handle fixed and dynamic cases)
        for (uint32_t i = 0; i < j.size(); ++i) {
            vector[i] = j[i];
        }
    }
};

//--------------------------------------------------------------------------------------------------
// serialization of matrices
template <typename T, int32_t N1, int32_t N2>
struct adl_serializer<Eigen::Matrix<T, N1, N2>> {
    static void to_json(json& j, Eigen::Matrix<T, N1, N2> const& matrix) {
        for (uint32_t row = 0; row < matrix.rows(); ++row) {
            json row_json;
            for (uint32_t col = 0; col < matrix.cols(); ++col) {
                row_json.push_back(matrix(row, col));
            }
            j.push_back(row_json);
        }
    }

    /*
    static void from_json(json const& j, Eigen::Matrix<T, N, 1>& vector) {
        // TODO check size of matrix (have to handle fixed and dynamic cases)
        for (uint32_t i = 0; i < j.size(); ++i) {
            vector[i] = j[i];
        }
    }
    */
};

//--------------------------------------------------------------------------------------------------
// serialization of matrix tranposes
template <typename T, int32_t N1, int32_t N2>
struct adl_serializer<optimization::EigenMatrixTranspose<T, N1, N2>> {
    static void to_json(json& j, optimization::EigenMatrixTranspose<T, N1, N2> const& matrix) {
        for (uint32_t row = 0; row < matrix.matrix.cols(); ++row) {
            json row_json;
            for (uint32_t col = 0; col < matrix.matrix.rows(); ++col) {
                row_json.push_back(matrix.matrix(col, row));
            }
            j.push_back(row_json);
        }
    }
};

}

#endif
