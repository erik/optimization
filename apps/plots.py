import argparse
import json
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

def paraboloid(x, y):
    return x**2 + y**2

def rosenbrock(x, y):
    return (1 - x) * (1 - x) + 100 * (y - x**2) * (y - x**2);

class OffsetLogNorm(mpl.colors.Normalize):
    def __init__(self, vmin=None, vmax=None, offset=None, clip=False):
        self.offset = offset
        mpl.colors.Normalize.__init__(self, np.log(vmin + self.offset), np.log(vmax + self.offset), clip)

    def __call__(self, value, clip=None):
        return mpl.colors.Normalize.__call__(self, np.ma.log(value + self.offset))

def rosenbrock_plot(x_min, x_max, y_min, y_max):
    fig = plt.figure(figsize=(8,7))
    left, bottom, width, height = 0.1, 0.1, 0.86, 0.8
    ax = fig.add_axes([left, bottom, width, height])
    ax.set_aspect(1)
    #ax.set_xlabel('x (cm)')
    #ax.set_ylabel('y (cm)')

    n_values = 800
    x_vals = np.linspace(x_min, x_max, n_values)
    y_vals = np.linspace(y_min, y_max, n_values)
    X, Y = np.meshgrid(x_vals, y_vals)
    Z = rosenbrock(X, Y)

    levels = [1e-2, 1e-1, 1e0, 1e1, 1e2, 1e3, 1e4, 1e5]
    ax.contour(X, Y, Z, levels, colors='0.5', linewidths=0.5)
    contour_filled = ax.contourf(X, Y, Z, levels,
        cmap='plasma',
        norm=OffsetLogNorm(vmin=Z.min(), vmax=Z.max(), offset = 1)
        #norm=mpl.colors.LogNorm(vmin=Z.min(), vmax=Z.max())
    )
    fig.colorbar(contour_filled, format=mpl.ticker.LogFormatterSciNotation())
    return fig, ax

def add_points(fig, ax, points):
    x = [p[0] for p in points]
    y = [p[1] for p in points]
    ax.plot(x, y, 'k.')
    return fig, ax

# polygons is a list of numpy arrays, for which each row gives a vertex
def add_polygons(fig, ax, polygons):
    for polygon in polygons:
        # only plot first two dims
        p = mpl.patches.Polygon(np.array(polygon)[:,0:2], True, fill=False, color="black", zorder=2)
        ax.add_patch(p)
        # theoretically patch collections could be more efficient
        #p = PatchCollection(patches, alpha=0.4)
        #ax.add_collection(p)
    return fig, ax

# points is a numpy array of dim n x 2 e.g. np.array([[0, 0], [1, 0], [1, 1]])
def add_lines(fig, ax, points, closed=False):
    p = mpl.patches.Polygon(points, closed, fill=False, color="black")
    ax.add_patch(p)
    #p = PatchCollection(patches, alpha=0.4)
    #ax.add_collection(p)
    return fig, ax

def add_circles(fig, ax, centers, radii):
    for center, r in zip(centers, radii):
        ax.add_artist(plt.Circle(center, r, color='k', fill=False))
    return fig, ax


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Generate a plot from a visualization file.')
    parser.add_argument(
        'vis_file',
        type=argparse.FileType('r'),
        help='optimization visualization file'
    )
    parser.add_argument(
        'plot_file',
        help='where to save the plot'
    )
    args = parser.parse_args()

    vis_json = json.load(args.vis_file)
    title = vis_json["algorithm"]
    objective = vis_json["objective"]
    data = vis_json["data"]

    if objective == "rosenbrock":
        fig, ax = rosenbrock_plot(-2.5, 2.5, -2.5, 2.5)
    # need to implement this
    #elif objective == "paraboloid":
    #    fig, ax = paraboloid_plot(-3, 3, -3, 3)
    else:
        print(f"Error: Unrecognized objective: {objective}")
        exit(1)

    if "points" in data:
        fig, ax = add_points(fig, ax, data["points"])

    if "polygons" in data:
        fig, ax = add_polygons(fig, ax, data["polygons"])

    # Haven't tested this yet
    #if "cirlces" in data:
    #    fig, ax = add_circles(fig, ax, data["cirlces"])

    ax.set_title(title)
    #plt.show(fig)
    plt.savefig(args.plot_file)
