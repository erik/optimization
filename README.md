# Optimization

My toolbox of optimization algorithms, with visualization utilities.

## Dependencies

- a modern C++ compiler (i.e. one that supports C++17 features, ex. gcc 7+)
- cmake 3.13 or newer
- Eigen3 (installable via apt, yum, or homebrew)

## Building

From the project's root directory,

```
mkdir build
cd build
cmake ..
make
```

Tips:
- you can build using multiple threads at once using e.g. `make -j4`
- you can specify a specific compiler using `cmake .. -DCMAKE_CXX_COMPILER=g++-8`
- for debugging symbols use `cmake .. -DCMAKE_BUILD_TYPE=Debug`
