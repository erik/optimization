#include "catch.hpp"
#include "optimizers/line_search/brent.h"

using namespace optimization;

//--------------------------------------------------------------------------------------------------
TEST_CASE("Brent", "[Brent]") {
    // The 1e-8 sets the absolute tolerance on the width of the bracket.
    // There's still a default relative tolerance in effect as well, but these tests don't hit it.
    Brent brent(1e-8);
    Bracket bracket;
    Sample<Scalar> result;

    SECTION("parabola") {
        struct Parabola {
            void operator()(Scalar x, Scalar& y) const { y = x * x; }
        };
        Parabola parabola;

        bracket = Bracket(-2, -1, 2, 4, 1, 4);
        result = brent.optimize(parabola, bracket);
        // The parabola is flat here so y accuracy had better exceed x accuracy.
        REQUIRE(std::abs(result.point) < 1e-8);
        REQUIRE(std::abs(result.value) < 1e-16);
        // Just a sanity check.
        REQUIRE(brent.n_evaluations() < 100);

        bracket = Bracket(50, 10, -100, 2500, 100, 10000);
        result = brent.optimize(parabola, bracket);
        // The parabola is flat here so y accuracy had better exceed x accuracy.
        REQUIRE(std::abs(result.point) < 1e-8);
        REQUIRE(std::abs(result.value) < 1e-16);
        // Just a sanity check.
        REQUIRE(brent.n_evaluations() < 100);
    }
}
