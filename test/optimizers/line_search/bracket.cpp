#include "catch.hpp"
#include "optimizers/line_search/bracket.h"

using namespace optimization;

//--------------------------------------------------------------------------------------------------
TEST_CASE("Bracket", "[Bracket]") {
    BracketFinder bracket;

    SECTION("parabola") {
        struct Parabola {
            void operator()(Scalar x, Scalar& y) const { y = x * x; }
        };
        Parabola parabola;

        bracket.bracket(parabola, -2, -1);
        // Necessary invariant: we have a bracket.
        REQUIRE(bracket.y_2() < bracket.y_1());
        REQUIRE(bracket.y_2() < bracket.y_3());
        // Because this is a parabola, it should find the minimum exactly.
        REQUIRE(bracket.y_2() < 1e-9);
        // The points should have this ordering because we started searching to the left of the min.
        REQUIRE(bracket.x_1() < bracket.x_2());
        REQUIRE(bracket.x_2() < bracket.x_3());
        // Here it does a golden ratio step then completes the bracket with interpolation.
        // So two initial evaluations, plus two more, for four total.
        REQUIRE(bracket.n_evaluations() == 4);

        bracket.bracket(parabola, 20, 22);
        // Necessary invariant: we have a bracket.
        REQUIRE(bracket.y_2() < bracket.y_1());
        REQUIRE(bracket.y_2() < bracket.y_3());
        // Because this is a parabola, it should find the minimum exactly.
        REQUIRE(bracket.y_2() < 1e-9);
        // Here we started searching to the right of the min.
        REQUIRE(bracket.x_1() > bracket.x_2());
        REQUIRE(bracket.x_2() > bracket.x_3());
        // Here it does a golden ratio step, interpolation step, and golden ratio step.
        // So two initial evaluations, plus three more, for five total.
        REQUIRE(bracket.n_evaluations() == 5);
    }
}
