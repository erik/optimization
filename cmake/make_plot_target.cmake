#set(PLOT_TARGETS "")

# TODO Enable arguments to be passed to the C++ executable
function(make_plot_target TARGET ID)
    cmake_parse_arguments(PLOT "" "" "ARGS" ${ARGN})
    add_custom_command(
        OUTPUT ${TARGET}_log_${ID}.json ${TARGET}_vis_${ID}.json
        COMMAND ${TARGET} ${TARGET}_log_${ID}.json ${TARGET}_vis_${ID}.json ${PLOT_ARGS}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        DEPENDS ${TARGET}
    )

    add_custom_command(
        OUTPUT ${TARGET}_plot_${ID}.pdf
        COMMAND python3 ${CMAKE_SOURCE_DIR}/apps/plots.py ${TARGET}_vis_${ID}.json ${TARGET}_plot_${ID}.pdf
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        DEPENDS ${CMAKE_SOURCE_DIR}/apps/plots.py ${TARGET}_vis_${ID}.json
    )

    add_custom_target(${TARGET}_plot_${ID}
        DEPENDS ${TARGET}_plot_${ID}.pdf
    )

    #message(STATUS "another one")
    #list(APPEND PLOT_TARGETS ${TARGET}_plot_${ID})
    #set(PLOT_TARGETS ${PLOT_TARGETS} PARENT_SCOPE)
    #message(STATUS ${PLOT_TARGETS})
endfunction()

function(make_meta_plot_target)
    # TODO Don't hard code this.
    add_custom_target(all_plots)
    add_dependencies(all_plots gradient_descent_plot_2d)
    add_dependencies(all_plots gradient_descent_plot_10d)
    add_dependencies(all_plots conjugate_gradient_descent_plot_2d)
    add_dependencies(all_plots conjugate_gradient_descent_plot_10d)
    add_dependencies(all_plots nelder_mead_plot_2d)
    add_dependencies(all_plots nelder_mead_plot_10d)
    add_dependencies(all_plots cma_es_plot_2d)
    add_dependencies(all_plots cma_es_plot_10d)
endfunction()
