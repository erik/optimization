# This file defines an interface library used to add common compile flags to all libraries and
# executables.

#---------------------------------------------------------------------------------------------------
add_library(shared_settings INTERFACE)
target_compile_features(shared_settings INTERFACE cxx_std_17)

# Speed flags
target_compile_options(shared_settings INTERFACE -march=native -ffast-math)

# Warning flags
target_compile_options(shared_settings INTERFACE
    -Wall
    -Wcast-align
    -Wcast-qual
    -Wextra
    -Wundef
    -Wzero-as-null-pointer-constant
    -pedantic
)
